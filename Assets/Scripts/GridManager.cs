using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//questo script � presente in un empty chiamato GridGenerator e gestisce lo spawn e il riordinamento dei pezzi
public class GridManager : MonoBehaviour
{
    [HideInInspector] public FoodPieces[,] myGrid = new FoodPieces[sizeX, sizeY];   //questo array multidimensionale sar� la mia griglia
    [HideInInspector] public List<Transform> middleLower = new List<Transform>();   //questa lista controlla i pezzi che spawnano alla riga 1 (per evitare righe vuote)
    [HideInInspector] public List<Transform> middleUpper = new List<Transform>();   //questa lista controlla i pezzi che spawnano alla riga 2 (per evitare righe vuote)

    public GameObject[] foodPrefabs;                            //ho un array di prefab tra cui scegliere il cibo da spawnare

    [HideInInspector] public bool reordered;                    //questa booleana diventa true solo quando tutti i pezzi sono stati riordinati

    const int sizeX = 4;                                        //do una size in X alla griglia = 4
    const int sizeY = 4;                                        //do una size in X alla griglia = 4

    public LayerMask foodMask;                                  //setto la LayerMask delle fette di cibo
    int randomizer = 0;                                         //questo intero mi servir� come percentuale di spawn
    int breadCounter = 0;                                       //questa � la quantit� di fette di pane spawnate
    float offset = 1.15f;                                       //creo un offset per distanziare le fette
    float timer;                                                //il timer mi serve per richiamare i vari check in momenti diversi
    bool alreadyChecked;                                        //questa booleana diventa true solo quando � stato fatto il primo check dei pezzi

    //in Awake inizializzo la matrice di pezzi
    private void Awake()
    {
        InizializeMatrix();                                     
    }

    //faccio partire il timer e prima faccio il controllo le fette di pane (a maggior priorit�), poi quelle degli ingredienti (a minor priorit�)
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 1f && !alreadyChecked)
        {
            CheckIfSamePositionBread();
        }
        if (timer >= 2f && !reordered)
            ReorderFood();
    }

    //inizializzo la matrice con un doppio for innestato nel quale la x � il numero di colonne e la y il numero di righe
    void InizializeMatrix()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                SpawnFood(x, y);
            }
        }
    }

    //lo spawn delle fette avviene in posizioni randomiche tra quelle della griglia 4x4
    void SpawnFood(int x, int y)
    {
        //setto il randomizer ad un valore randomico tra 0 e 99
        randomizer = Random.Range(0, 100);

        //se � un valore compreso tra 50 e 99 spawnano fette di cibo, altrimenti no
        if (randomizer >= 50)
        {
            //do la priorit� alle due fette di pane, facendole spawnare per prime
            if (breadCounter < 2)
            {
                //nella lista di prefab l'elemento 0 � il pane, perci� lo spawno sulla griglia con un determinato offset e aumento il contatore
                GameObject tmp = Instantiate(foodPrefabs[0]);
                tmp.transform.localPosition = new Vector3(x * offset, y * offset, 0);
                breadCounter++;
            }
            else
            {
                //se ho gi� spawnato le fette di pane, posso spawnare randomicamente uno qualsiasi degli altri ingredienti
                int randomPrefab = Random.Range(1, 4);
                GameObject tmp = Instantiate(foodPrefabs[randomPrefab]);
                tmp.transform.localPosition = new Vector3(x * offset, y * offset, 0);
            }
        }

        //avvio un delay prima di fare il check delle posizioni delle fette di pane
        StartCoroutine(delayBeforeReorder());
    }

    //dopo lo spawn, controllo le fette di pane e le riordino
    private IEnumerator delayBeforeReorder()
    {
        yield return new WaitForSeconds(0.5f);
        ReorderBread();
    }

    //riordino le fette di pane in modo che siano sempre una sopra l'altra o una accanto all'altra
    void ReorderBread()
    {
        //creo delle variabili che si riferiscano alle posizioni sulla griglia della prima e della seconda fetta di pane (i primi due elementi della lista di spawn)
        float x1, y1, x2, y2;
        Transform firstBread, secondBread;
        firstBread = InputManager.Instance.pieces[0].transform;
        secondBread = InputManager.Instance.pieces[1].transform;
        x1 = firstBread.position.x;
        y1 = firstBread.position.y;
        x2 = secondBread.position.x;
        y2 = secondBread.position.y;

        //faccio gi� qui il controllo delle righe per assicurarmi che non ce ne siano di vuote
        for (int i = 0; i < InputManager.Instance.pieces.Count; i++)
        {
            Transform food;
            food = InputManager.Instance.pieces[i].transform;

            //se la fetta di cibo si trova alla riga 1 viene aggiunta alla sua lista
            if (food.position.y == 1 * offset && !middleLower.Contains(food))
                middleLower.Add(food);

            //se la fetta di cibo si trova alla riga 2 viene aggiunta alla sua lista
            if (food.position.y == 2 * offset && !middleUpper.Contains(food))
                middleUpper.Add(food);
        }

        //se la seconda fetta di pane si trova subito accanto alla prima (sulla stessa riga) non c'� bisogno di fare altri controlli
        if (x2 == x1 + offset && y1 == y2)
        {
            return;
        }
        //se le fette di pane non sono sulla stessa colonna, la seconda fetta si sposta a sinistra di uno
        else if (x1 != x2 && y1 != y2)
        {
            secondBread.position += offset * Vector3.left;
        }
        //se sono sulla stessa colonna
        else if (x1 == x2 && y1 != y2)
        {
            //nel caso in cui la seconda fetta sia pi� in basso della prima (ma non solo di una posizione) si muover� verso l'alto
            if (y2 < y1 && y2 != y1 - offset)
            {
                secondBread.position += offset * Vector3.up;
            }
            //se la seconda fetta � pi� in alto della prima (ma non solo di una posizione) si muover� verso il basso
            else if (y2 > y1 && y2 != y1 + offset)
            {
                secondBread.position += offset * Vector3.down;
            }
        }
    }

    //controllo se le fette di pane sono nella stessa posizione
    void CheckIfSamePositionBread()
    {
        //creo delle variabili che si riferiscano alle posizioni sulla griglia della prima e della seconda fetta di pane (i primi due elementi della lista di spawn)
        float y2;
        Transform firstBread, secondBread;
        firstBread = InputManager.Instance.pieces[0].transform;
        secondBread = InputManager.Instance.pieces[1].transform;
        y2 = secondBread.position.y;

        //se la prima fetta � nella stessa posizione della seconda fetta
        if (firstBread.position == secondBread.position)
        {
            //e le fette si trovano nell'ultima posizione in alto (faccio scendere la seconda)
            if (y2 == offset * (sizeY - 1))
            {
                secondBread.position += offset * Vector3.down;
            }
            //se invece le fette si trovano nella prima posizione in basso (faccio salire la seconda)
            else if (y2 == 0)
            {
                secondBread.position += offset * Vector3.up;
            }
            //se non si trovano n� alla prima n� all'ultima posizione, la seconda scende di default
            else
            {
                secondBread.position += offset * Vector3.down;
            }
        }

        //se la prima o la seconda fetta di pane � sulla riga pi� bassa e la riga 2 � vuota
        if ((firstBread.position.y == 0 || secondBread.position.y == 0) && middleUpper.Count == 0)
        {
            //prendo randomicamente una fetta di cibo e la spawno nella riga 2 sulla stessa colonna della prima fetta di pane
            int randomPrefab = Random.Range(1, 4);
            GameObject tmp = Instantiate(foodPrefabs[randomPrefab]);
            tmp.transform.localPosition = new Vector3(firstBread.position.x, 2 * offset, 0);
        }

        //se la prima o la seconda fetta di pane � sulla riga pi� alta e la riga 1 � vuota
        if ((firstBread.position.y == offset * (sizeY - 1) || secondBread.position.y == offset * (sizeY - 1)) && middleLower.Count == 0)
        {
            //prendo randomicamente una fetta di cibo e la spawno nella riga 1 sulla stessa colonna della prima fetta di pane
            int randomPrefab = Random.Range(1, 4);
            GameObject tmp = Instantiate(foodPrefabs[randomPrefab]);
            tmp.transform.localPosition = new Vector3(firstBread.position.x, 1 * offset, 0);
        }

        //termino il primo check (quello del pane e delle righe vuote)
        alreadyChecked = true;
    }

    //ora riordino le fette di cibo rimanenti
    void ReorderFood()
    {
        //creo delle variabili che si riferiscano alle posizioni sulla griglia della prima fetta di pane e uno qualsiasi degli altri ingredienti
        float x1, x2;
        Transform bread, food;
        bread = InputManager.Instance.pieces[0].transform;

        //parto dall'elemento 2 della lista perch� l'elemento 0 e l'elemento 1 sono fette di pane
        for (int i = 2; i < InputManager.Instance.pieces.Count; i++)
        {
            food = InputManager.Instance.pieces[i].transform;
            x1 = bread.position.x;
            x2 = food.position.x;

            //se la fetta di pane si trova pi� a sinistra della fetta di cibo e non sono una accanto all'altra
            if (x1 < x2 && !Physics.Raycast(food.position, Vector3.left, 1, foodMask))
            {
                //sposto la fetta di cibo verso sinistra
                food.position += offset * Vector3.left;
            }
        }

        //avvio una coroutine che pone fine alla fase di riordinamento (perch� potrebbe doverla rifare pi� volte)
        StartCoroutine(timeBeforeStopReorder());
    }

    //pongo fine al riordinamento
    IEnumerator timeBeforeStopReorder()
    {
        yield return new WaitForSeconds(0.3f);
        reordered = true;
    }

    //se una fetta finisce sul trigger di margine
    private void OnTriggerStay(Collider other)
    {
        //ed � una fetta di altri ingredienti, la rimuovo e la distruggo
        if (other.GetComponent<FoodPieces>().type == FoodType.Others)
        {
            InputManager.Instance.pieces.Remove(other.GetComponent<FoodPieces>());
            Destroy(other.gameObject);
        }
        //se � una fetta di pane
        else
        {
            //ed � la prima, la sposto sopra la seconda
            if (other.GetComponent<FoodPieces>() == InputManager.Instance.pieces[0])
                other.transform.position = InputManager.Instance.pieces[1].transform.position + new Vector3(0, offset, 0);

            //se invece � la seconda, la sposto sopra la prima
            if (other.GetComponent<FoodPieces>() == InputManager.Instance.pieces[1])
                other.transform.position = InputManager.Instance.pieces[0].transform.position + new Vector3(0, offset, 0);
        }
    }
}
