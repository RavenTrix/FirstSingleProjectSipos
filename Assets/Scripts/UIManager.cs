using UnityEngine;
using UnityEngine.SceneManagement;

//lo script si trova in scena in un empty chiamato UIManager e gestisce i pulsanti non correlati ai livelli o al movimento dei pezzi
public class UIManager : MonoBehaviour
{
    //questa � l'immagine della forchetta e il coltello che si sovrappone al piatto dell'Undo quando non ci sono mosse da annullare
    public GameObject undoButtonImage;

    private void Update()
    {
        UndoButton();
    }

    //il button Play che c'� nella scena StartScene, carica la scena successiva (quella dei livelli) chiudendo definitivamente quella di start
    public void PlayButton()
    {
        SceneManager.LoadScene("LevelScene");
    }

    //in questo metodo attivo e disattivo la forchetta e il coltello sopra il piatto dell'Undo
    public void UndoButton()
    {
        //se mi trovo nella scena dei livelli (quindi c'� l'InputManager) e non posso annullare la mossa perch� precedente all'ultima
        if (InputManager.Instance != null && !InputManager.Instance.canUndo)
        {
            //attivo la forchetta e il coltello
            undoButtonImage.SetActive(true);
        }
        else
        {
            //senn� li disattivo e il pulsante diventa di nuovo cliccabile
            undoButtonImage.SetActive(false);
        }

    }
}
