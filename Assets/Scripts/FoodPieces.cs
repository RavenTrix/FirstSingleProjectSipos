using System.Collections;
using UnityEngine;

//questo script � presente in ogni fetta di cibo e gestisce le informazioni specifiche per quella fetta

//creo un enum che suddivida le fette dei vari ingredienti da quelle di pane
public enum FoodType
{
    Bread,
    Others
}
public class FoodPieces : MonoBehaviour
{
    [HideInInspector] public Vector3 previousPosition;  //questa � la posizione precedente (per fare l'undo)
    [HideInInspector] public Vector3 currentPosition;   //questa � la posizione corrente del pezzo
    [HideInInspector] public GameObject parent;         //questa � la fetta di cibo che fa da parent a quelle impilate su di essa
    [HideInInspector] public bool selected;             //questa booleana specifica se la fetta � quella che si sta selezionando sul momento
    [HideInInspector] public bool current;              //questa booleana invece specifica se la fetta � quella corrente (anche se ho smesso di toccarla ma non ho ancora toccato altro)
    [HideInInspector] public bool stackingFood;         //ogni volta che una fetta si impila sulle altre questa booleana diventa true
    [HideInInspector] public bool onBread;              //ogni volta che una fetta si posiziona sul pane questa booleana diventa true
    [HideInInspector] public int nextPos;               //questo intero specifica la direzione presa dalla fetta
    [HideInInspector] public int numOfPiecesStacked;    //questa � la quantit� di fette impilate su quella attuale
    public LayerMask foodMask;                          //ho creato una LayerMask specifica per le fette di cibo per non interferire con il panel di sfondo
    public FoodType type;                               //gestisco un tipo di fetta tra quelle specificate nell'enum
    float timer;                                        //creo un timer che impedisca all'OnCollisionStay di agire dopo che le fette si sono posizionate correttamente

    private void Start()
    {
        InputManager.Instance.pieces.Add(this);         //aggiungo la fetta attuale alla lista di fette appena il livello viene caricato
        currentPosition = transform.position;           //setto la posizione corrente come quella attuale
        previousPosition = transform.position;          //e lo stesso faccio per la posizione precedente (visto che non ho ancora spostato la fetta manualmente)
    }
    private void Update()
    {
        timer += Time.deltaTime;                        //faccio partire il timer per l'OnCollisionStay
    }

    //faccio un metodo che controlli la posizione successiva (la direzione della fetta prima che si muova)
    public void CheckPosition()                         
    {
        RaycastHit hit;
        Vector3 direction;

        //assegno una direzione ad ogni numero che verr� settato nell'InputManager in base alla direzione del drag
        switch (nextPos)
        {
            case 1: direction = Vector3.right;
                break;
            case 2: direction = Vector3.left;
                break;
            case 3: direction = Vector3.up;
                break;
            default: direction = Vector3.down;
                break;
        }

        //faccio un check con il raycast nella direzione verso la quale la fetta dovrebbe muoversi
        if (Physics.Raycast(transform.position, direction, out hit, 1.5f, foodMask))
        {
            //se colpisce qualcosa
            if (hit.collider != null)
            {
                //le fette possono impilarsi e la fetta corrente setter� come parent la fetta colpita dal raycast
                stackingFood = true;
                parent = hit.transform.gameObject;

                //attendo il tempo necessario alla fetta per spostarsi e aggiorno la quantit� di fette impilate in quella posizione
                StartCoroutine(delayBeforeAddingPieces());
            }
        }
        else
        {
            //se non colpisce nessuna fetta, non si pu� impilare e quindi non si deve muovere
            stackingFood = false;
        }
    }

    //aggiorno la quantit� di fette del parent sommando la fetta appena impilata e quelle gi� presenti su di essa dopo lo spostamento
    IEnumerator delayBeforeAddingPieces()
    {
        yield return new WaitForSeconds(InputManager.Instance.timeToMove);
        parent.GetComponent<FoodPieces>().numOfPiecesStacked++;
        parent.GetComponent<FoodPieces>().numOfPiecesStacked += numOfPiecesStacked;
    }

    //entro i primi due secondi di assestamento delle fette, controllo se ce ne sono di sovrapposte
    private void OnCollisionStay(Collision collision)
    {
        //se non si tratta di pane (che non deve essere eliminato per nessun motivo, senn� non ci sarebbe una partita)
        if(timer<= 2 && collision.gameObject.GetComponent<FoodPieces>().type != FoodType.Bread)
        {
            //distruggo la fetta sulla quale si � sovrapposta quella attuale e la tolgo dalla lista
            InputManager.Instance.pieces.Remove(collision.gameObject.GetComponent<FoodPieces>());
            Destroy(collision.gameObject);
        }
    }
}
