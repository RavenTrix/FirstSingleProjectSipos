using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

//questo script si trova su un empty chiamato LevelManager e gestisce la fine del livello e il caricamento di quello nuovo
//contiene anche i metodi dei due button RESET e NEXTLEVEL
public class LevelManager : MonoBehaviour
{
    public RectTransform nextLevelCanvas;                      //questo � il panel di caricamento scena

    [HideInInspector]public List<Vector3> startPositions;      //questa sar� una lista di posizioni iniziali di ogni singolo pezzo
    List<FoodPieces> foodList;                                 /*questa sar� la lista di fette di cibo alle quali assegner� la posizione iniziale 
                                                               (creata per non dover richiamare l'istanza dell'InputManager troppe volte)*/

    float timeOfFade = 4f;                                     //questo � il tempo che do alla schermata di caricamento per dissolversi
    float timer;                                               //creo un timer che funga da delay prima di passare al livello successivo

    private void Start()
    {
        StartCoroutine(delay());           //richiamo in start il delay che gestisce il fade out della schermata di caricamento
        StartCoroutine(startDelay());      /*e poi il delay prima che si settino le posizioni iniziali 
                                            (visto che i pezzi devono prima riposizionarsi in maniera corretta)*/
    }

    private void Update()
    {
        //per ogni fetta inserita nella lista dell'InputManager
        foreach (FoodPieces food in InputManager.Instance.pieces)
        {
            //se la quantit� di fette sopra il pane � pari a quella totale delle fette di cibo meno le due fette di pane e il pane risulta essere sopra al pane
            if (InputManager.Instance.onBreadPieces.Count == InputManager.Instance.pieces.Count - 2 && food.type == FoodType.Bread && food.onBread)
            {
                //parte un timer (che serve principalmente a vedere il sandwitch prendere forma) prima di caricare la stessa scena per un nuovo livello
                timer += Time.deltaTime;
                if (timer >= 1)
                    SceneManager.LoadScene("LevelScene");
            }
        }
    }


    //questo � il primo delay che viene richiamato e che gestisce con DoTween il fadeOut della schermata di caricamento e che richiama a sua volta il secondo delay
    IEnumerator delay()
    {
        yield return new WaitForSeconds(2);
        nextLevelCanvas.GetComponent<CanvasGroup>().DOFade(0f, timeOfFade);
        StartCoroutine(delayBeforeFadeOut());
    }

    //questo delay attende che il fadeOut sia terminato prima di disattivare il panel che funge da schermata di caricamento
    IEnumerator delayBeforeFadeOut()
    {
        yield return new WaitForSeconds(timeOfFade);
        nextLevelCanvas.gameObject.SetActive(false);
    }

    //questo delay attende che i pezzi si siano riposizionati nella posizione corretta prima di inserire le loro posizioni iniziali in una lista
    IEnumerator startDelay()
    {
        yield return new WaitForSeconds(2.5f);
        foodList = InputManager.Instance.pieces;
        for (int i = 0; i < foodList.Count; i++)
        {
            startPositions.Add(foodList[i].transform.position);
        }
    }


    //questo � il button NEXTLEVEL che si vede nella scena dei livelli di gioco e ricarica la scena corrente (facendo ripartire lo spawn randomico dei pezzi)
    public void NewLevel()
    {
        string currentLevel = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentLevel);
    }

    //questo � il button RESETLEVEL che si vede nella scena dei livelli di gioco e resetta tutte le booleane, le posizioni e le "parentele" tra fette di cibo
    public void Reset()
    {
        for (int i = 0; i < foodList.Count; i++)
        {
            //setto ad ogni fetta di cibo la posizione attuale, corrente e precedente uguale a quella iniziale
            foodList[i].transform.position = startPositions[i];
            foodList[i].currentPosition = startPositions[i];
            foodList[i].previousPosition = startPositions[i];

            //riattivo il boxCollider disattivato in precedenza per avere accesso alla fetta giusta tra quelle impilate con il raycast
            foodList[i].GetComponent<BoxCollider>().enabled = true;

            //resetto la posizione successiva per non rischiare movimenti dettati da precedenti istruzioni
            foodList[i].nextPos = 0;

            //setto a false le booleane che controllavano le pile di cibo, la fetta corrente, quella selezionata e quella sul pane
            foodList[i].stackingFood = false;
            foodList[i].current = false;
            foodList[i].selected = false;
            foodList[i].onBread = false;

            //stacco ogni pezzo dal proprio parent, in modo che possa muoversi di nuovo liberamente e setto la quantit� di pezzi impilati a zero
            foodList[i].transform.SetParent(null);
            foodList[i].numOfPiecesStacked = 0;

            //tolgo dalla lista di pezzi sulle fette di pane e rendo impossibile fare l'undo (visto che non ci sono azioni da annullare a inizio gioco)
            InputManager.Instance.onBreadPieces.Clear();
            InputManager.Instance.canUndo = false;
        }
    }



}
