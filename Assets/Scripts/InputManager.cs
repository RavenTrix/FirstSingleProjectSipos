using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

//questo script si pu� trovare in un empty chiamato InputManager e gestisce il touch e il movimento dei pezzi
public class InputManager : MonoBehaviour
{
    //creo un'istanza di questo script in modo da accedervi pi� facilmente (singleton pattern)
    static InputManager _instance;
    public static InputManager Instance { get => _instance; private set => _instance = value; }


    [HideInInspector] public List<FoodPieces> pieces;                                   //creo una lista che si autofilla con le fette di cibo appena spawnate
    [HideInInspector] public List<FoodPieces> onBreadPieces = new List<FoodPieces>();   //questa lista contiene le fette di cibo che si trovano sopra al pane
    [HideInInspector] public List<Transform> bread = new List<Transform>();             //questa lista contiene le due fette di pane
    [HideInInspector] public bool canUndo;                                              //questa booleana consente o non consente di annullare un'azione
    [HideInInspector] public float offset = 0.7f;                                       //questa � la distanza che ogni pezzo deve percorrere per spostarsi
    [HideInInspector] public float timeToMove = 0.5f;                                   //questo � il tempo che impiega un pezzo a spostarsi
    public LayerMask foodMask;                                                          //questa � la layerMask delle fette di cibo
    Vector2 touchStartPos, touchEndPos, endPos;                                         //definisco la posizione iniziale e finale di touch e la posizione finale del pezzo
    IEnumerator playCoroutine;                                                          //definisco la coroutine da richiamare per muovere il pezzo
    bool canMove;                                                                       //se la booleana � true la fetta pu� muoversi senn� non pu�
    float multiplier = 0.05f;                                                           //questo � il valore che va moltiplicato all'offset per impilare le fette
    float timer;                                                                        //creo un timer dopo il quale la fetta non � pi� quella corrente

    //mi assicuro che questa sia l'unica istanza esistente in scena dell'InputManager
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
    }

    private void Update()
    {
        //controllo il touch
        GetInputTouch();


        for (int i = 0; i < pieces.Count; i++)
        {
            FoodPieces food = pieces[i].GetComponent<FoodPieces>();

            //ruoto ogni fetta di cibo selezionata verso il suo futuro parent ma ruoto di 180� in modo che il suo forward sia invertito
            if (food.selected && food.parent != null && canMove)
            {
                food.transform.LookAt(food.parent.transform);
                food.transform.Rotate(0, 180, 0);
            }
            //se esiste gi� un parent e la fetta ha il collider disabilitato, come quello del parent
            if (food.parent != null)
            {
                if (!food.GetComponent<BoxCollider>().enabled && !food.parent.GetComponent<BoxCollider>().enabled)
                {
                    //dopo qualche istante la fetta "figlia" non sar� pi� current
                    //questo perch� quando ho gi� due pezzi impilati e devo spostarli sopra un terzo, non voglio che quei due pezzi continuino ad essere current
                    //diversamente, facendo l'undo ed essendo tutti ancora current, tornerebbero tutti alla loro posizione precedente (non solo l'ultimo impilato)
                    timer += Time.deltaTime;
                    if (timer >= 0.5f)
                        food.current = false;
                }
            }
        }

    }
    void GetInputTouch()
    {
        //per ogni pezzo di cibo auto-inserito nella lista relativa alla scena
        for (int i = 0; i < pieces.Count; i++)
        {
            FoodPieces foodPiece = pieces[i].GetComponent<FoodPieces>();

            //aggiungo alla lista delle fette di pane solo le due fette di pane presenti in scena
            if (foodPiece.type == FoodType.Bread && !bread.Contains(foodPiece.transform))
            {
                bread.Add(foodPiece.transform);
            }

            //se non tocco niente, skippo le istruzioni successive, senn� prendo il primo touch
            if (Input.touchCount <= 0) return;
            Touch touch = Input.GetTouch(0);

            //se ho cominciato a premere su un'area dello schermo 
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;

                //setto la booleana selected relativa a quel pezzo di cibo a true se ho cliccato nella sua area
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, foodMask))
                {
                    if (hit.collider != null && hit.transform.gameObject == foodPiece.gameObject)
                    {
                        foodPiece.selected = true;

                        //se ho appena cominciato a cliccare e il pezzo � gi� selezionato lo faccio diventare current
                        if (touch.phase == TouchPhase.Began)
                        {
                            foreach (FoodPieces piece in pieces)
                            {
                                if (piece.selected)
                                    piece.current = true;
                                else
                                    piece.current = false;
                            }
                        }
                    }
                    else
                    {
                        //setto la booleana selected relativa a quel pezzo di cibo a false se ho cliccato fuori
                        foodPiece.selected = false;
                    }
                }
                else
                {
                    //setto la booleana selected relativa a quel pezzo di cibo a false se ho cliccato fuori
                    foodPiece.selected = false;
                }
            }
            //se la fetta � stata selezionata
            if (foodPiece.selected)
            {
                //appena clicco sulla fetta di cibo setto il primo punto per definire una direzione e la sua posizione precedente per l'undo
                if (touch.phase == TouchPhase.Began)
                {
                    touchStartPos = touch.position;
                    foodPiece.previousPosition = foodPiece.transform.position;
                }

                //se ho smesso di toccare la fetta
                else if (touch.phase == TouchPhase.Ended)
                {
                    //posso annullare l'azione
                    canUndo = true;

                    //appena rilascio setto il secondo punto per definire la direzione
                    touchEndPos = touch.position;

                    //faccio la sottrazione tra punto finale e punto iniziale per definire il segno
                    float deltaX = touchEndPos.x - touchStartPos.x; // (dx/sx)
                    float deltaY = touchEndPos.y - touchStartPos.y; //(sopra/sotto)

                    //definisco l'offset verticale e l'offset orizzontale
                    Vector2 verticalOffset = new Vector2(0, offset);
                    Vector2 horizontalOffset = new Vector2(offset, 0);

                    //se il movimento del mio touch � troppo piccolo, la fetta non si muove
                    if (Mathf.Abs(deltaX) <= 0.2f && Mathf.Abs(deltaY) <= 0.2f) return;

                    //se il movimento del touch � pi� grande sull'asse X che sull'asse Y
                    else if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
                    {
                        //scelgo la direzione (sull'asse x) il verso (da 1 a 2 = dx o sx) con un offset definito
                        ChooseDirection(deltaX, 1, foodPiece, horizontalOffset);
                    }

                    //se il movimento del touch � pi� piccolo sull'asse X che sull'asse Y
                    else if (Mathf.Abs(deltaX) <= Mathf.Abs(deltaY))
                    {
                        //scelgo la direzione (sull'asse x) il verso (da 1 a 2 = su o gi�) con un offset definito
                        ChooseDirection(deltaY, 3, foodPiece, verticalOffset);
                    }

                    //controllo se le fette sono sul pane o no
                    CheckPiecesPosition(foodPiece);

                    //controllo se posso muovermi (se la destinazione non presenta fette non posso, se invece ci sono fette posso)
                    CheckIfCanMove(foodPiece);
                }

                //se � gi� stato fatto il calcolo della posizione finale, la fetta pu� muoversi
                if (canMove)
                {
                    //setto la currentPosition a quella di destinazione
                    foodPiece.currentPosition = endPos;

                    //richiamo la coroutine che definisce il movimento della fetta
                    playCoroutine = Move(foodPiece);
                    StartCoroutine(playCoroutine);

                    //concludo la sovrapposizione di pezzi
                    foodPiece.stackingFood = false;

                    //setto come parent della fetta attuale quella di destinazione
                    foodPiece.transform.SetParent(foodPiece.parent.transform);

                    //disattivo il collider della fetta appena impilata cos� non posso pi� spostare la fetta nel singolo ma solo tramite il parent
                    foodPiece.GetComponent<BoxCollider>().enabled = false;
                }
            }
        }
    }

    //muovo la fetta di cibo
    IEnumerator Move(FoodPieces food)
    {
        //creo un offset sull'asse z che equivale al numero di pezzi impilati sul parent + quelli impilati sulla fetta attuale + il parent stesso
        int zOffset = food.parent.GetComponent<FoodPieces>().numOfPiecesStacked + food.GetComponent<FoodPieces>().numOfPiecesStacked + 1;

        //muovo la fetta verso la sua posizione finale
        food.transform.DOMove((Vector3)endPos + new Vector3(0, 0, -multiplier * zOffset), timeToMove);

        yield return new WaitForSeconds(timeToMove);

        //setto il movimento della fetta a false
        canMove = false;
    }

    //controllo la posizione delle fette di cibo rispetto al pane
    void CheckPiecesPosition(FoodPieces food)
    {
        Vector2 breadPos0 = bread[0].position;
        Vector2 breadPos1 = bread[1].position;
        Vector2 distance0 = (Vector2)food.transform.position - breadPos0;
        Vector2 distance1 = (Vector2)food.transform.position - breadPos1;

        //se un ingrediente � vicinissimo (sovrapposto) ad una delle fette di pane
        if (food.type == FoodType.Others && (distance0.magnitude < 1.15f || distance1.magnitude < 1.15f))
        {
            //risulta sopra al pezzo di pane
            food.onBread = true;

            //e viene aggiunto alla lista
            if (!onBreadPieces.Contains(food))
            {
                onBreadPieces.Add(food);
            }
        }
        //se una fetta di pane risulta vicinissima (sovrapposta) ad un'altra fetta di pane la sua booleana diventa true
        if (food.type == FoodType.Bread && (distance0.magnitude < 1.15f || distance1.magnitude < 1.15f))
        {
            food.onBread = true;
        }
    }

    //controllo se posso spostare la fetta o no
    void CheckIfCanMove(FoodPieces foodPiece)
    {
        //se le fette non sono ancora tutte sul pane
        if (onBreadPieces.Count != pieces.Count - 2)
        {
            //la fetta pu� muoversi se non � pane e c'� qualcosa in quella direzione
            if (foodPiece.stackingFood && foodPiece.type != FoodType.Bread)
            {
                canMove = true;

                //setto onBread a true e aggiungo il pezzo alla lista anche se non � ancora ufficialmente sul pane
                //in questo modo quando il futuro parent finisce sul pane, i figli saranno gi� nella lista senza per forza essere i pezzi selezionati
                foodPiece.onBread = true;
                onBreadPieces.Add(foodPiece);
            }
            //se � pane o non c'� niente in quella direzione non pu� muoversi
            else
            {
                canMove = false;
            }
        }
        //se le fette sono tutte sul pane
        else
        {
            //se c'� effettivamente qualcosa nella posizione di destinazione, la fetta pu� muoversi (anche se � pane)
            if (foodPiece.stackingFood)
                canMove = true;
            //se non c'� niente in quella direzione non pu� muoversi
            else
                canMove = false;
        }
    }

    //scelgo la direzione verso la quale spostare la fetta
    void ChooseDirection(float delta, int nextPos, FoodPieces foodPiece, Vector2 offset)
    {
        int sign;

        //se la differenza tra la posizione finale e quella iniziale � maggiore di zero
        if (delta > 0)
        {
            //la fetta va verso destra se nextPos � 1 e su se nextPos � 3
            sign = 1;
            foodPiece.nextPos = nextPos;
        }
        //in caso contrario
        else
        {
            //la fetta va verso sinistra se nextPos � 1 e gi� se nextPos � 3
            sign = -1;
            foodPiece.nextPos = nextPos + 1; // (1 = destra) + 1 = 2 --> sinistra  ,  (3 = su) + 1 = 4 --> gi�
        }

        //la posizione finale della fetta � dato dalla sua stessa posizione pi� o meno (in base al segno) l'offset
        endPos = (Vector2)foodPiece.transform.position + sign * offset;

        //controllo se nella direzione che ho scelto c'� qualcosa o no
        foodPiece.CheckPosition();
    }

    //il tasto undo torna indietro di una sola mossa (l'ultima)
    public void Undo()
    {
        //se ho fatto almeno una mossa dall'inizio, dal reset o dall'ultimo undo
        if (canUndo)
        {
            for (int i = 0; i < pieces.Count; i++)
            {
                FoodPieces foodPiece = pieces[i].GetComponent<FoodPieces>();

                //se la fetta � quella corrente
                if (foodPiece.current)
                {
                    //tolgo al contatore di fette impilate sul parent quelle del child da riportare alla posizione precedente
                    foodPiece.parent.GetComponent<FoodPieces>().numOfPiecesStacked -= foodPiece.numOfPiecesStacked + 1;

                    //stacco la fetta dal suo parent
                    foodPiece.transform.SetParent(null);

                    //setto la posizione precedente come destinazione e controllo se posso muovermi
                    endPos = foodPiece.previousPosition;
                    foodPiece.CheckPosition();

                    //la fetta si muove e resetta la propria currentPosition e previousPosition
                    canMove = true;
                    foodPiece.currentPosition = endPos;
                    foodPiece.previousPosition = endPos;
                    foodPiece.transform.DOMove((Vector3)endPos, timeToMove);
                    canMove = false;

                    //resetto le principali booleane della fetta in modo che si trovi come in una condizione iniziale
                    foodPiece.stackingFood = false;
                    foodPiece.onBread = false;

                    //ripristino il boxCollider che devo poter colpire con il raycast di nuovo
                    foodPiece.GetComponent<BoxCollider>().enabled = true;

                    //rimuovo la fetta dalla lista di pezzi sopra il pane (o sopra le altre fette)
                    onBreadPieces.Remove(foodPiece);

                    //dopo alcuni secondi la fetta non � pi� quella corrente
                    StartCoroutine(delayBeforeReset(foodPiece));
                }
            }
        }
        //dopo aver fatto l'undo, devo fare una mossa per poterlo rifare
        canUndo = false;
    }

    //la fetta attuale non � pi� quella corrente dopo aver fatto l'undo
    IEnumerator delayBeforeReset(FoodPieces food)
    {
        yield return new WaitForSeconds(1f);
        food.current = false;
    }
}